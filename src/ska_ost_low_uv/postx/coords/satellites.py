"""satellites.py - Utilities for loading satellite positions."""
from __future__ import annotations

import typing

from astropy import units as u
from astropy.coordinates import (
    ICRS,
    ITRS,
    TEME,
    AltAz,
    CartesianDifferential,
    CartesianRepresentation,
    EarthLocation,
    SkyCoord,
)
from sgp4.api import Satrec

if typing.TYPE_CHECKING: # pragma: no cover
    from ska_ost_low_uv.postx import ApertureArray


def compute_satellite_altaz(aa: ApertureArray, satellite: Satrec) -> AltAz:
    """Compute the ALT/AZ for a satellite viewed by an aperture array.

    Args:
        aa (ApertureArray): ApertureArray object
        satellite (Satrec): SGP4 Satrec ephemeris

    Returns:
        altaz (AltAz): Apparent satellite position in altitude-azimuth
    """
    t = aa.t[aa.idx['t']]

    error_code, teme_p, teme_v = satellite.sgp4(t.jd1, t.jd2)  # in km and km/s
    teme_p = CartesianRepresentation(teme_p*u.km)
    teme_v = CartesianDifferential(teme_v*u.km/u.s)
    teme = TEME(teme_p.with_differentials(teme_v), obstime=t)
    itrs_geo = teme.transform_to(ITRS(obstime=t))
    topo_itrs_repr = itrs_geo.cartesian.without_differentials() - aa.earthloc.get_itrs(t).cartesian

    itrs_topo = ITRS(topo_itrs_repr, obstime=t, location=aa.earthloc)
    altaz  = itrs_topo.transform_to(AltAz(location=aa.earthloc, obstime=t))

    return altaz


def compute_satellite_radec(aa: ApertureArray, satellite: Satrec) -> SkyCoord:
    """Compute the RA/DEC for a satellite viewed by an aperture array.

    Args:
        aa (ApertureArray): ApertureArray object
        satellite (Satrec): SGP4 Satrec ephemeris

    Returns:
        radec (SkyCoord): Apparent satellite position in RA/DEC
    """
    t = aa.t[aa.idx['t']]
    altaz = compute_satellite_altaz(aa, satellite)
    # This next line seems necessary -- removes all other info apart from Alt/Az
    # I think this is needed to give 'apparent RA' instead of earth-centred?
    _altaz = AltAz(az=altaz.az, alt=altaz.alt, obstime=t, location=aa.earthloc)
    radec = SkyCoord(_altaz.transform_to(ICRS()))

    return radec


def load_tles(filename: str) -> dict:
    """Load the TLE data, assuming three lines per entry.

    Args:
        filename (str): Name of file to read.

    Returns:
        satdict (dict): Dictionary of satellite SGP4 Satrec objects
    """
    with open(filename, 'r') as fh:
        lines = fh.readlines()
    satdict = {}

    for ii in range(len(lines) // 3):
        name = lines[3*ii].strip()
        l1   = lines[3*ii+1].strip()
        l2   = lines[3*ii+2].strip()
        satdict[name] = Satrec.twoline2rv(l1, l2)
    return satdict
