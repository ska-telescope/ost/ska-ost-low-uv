import numpy as np
import numpy.typing as npt
from pyuvdata.analytic_beam import UnpolarizedAnalyticBeam


class SimpleCosSqBeam(UnpolarizedAnalyticBeam):
    """A zenith pointed cos-squared beam."""
    def _power_eval(
        self,
        *,
        az_grid: npt.NDArray[float], # type: ignore
        za_grid: npt.NDArray[float], # type: ignore
        f_grid: npt.NDArray[float], # type: ignore
    ) -> npt.NDArray[float]: # type: ignore
        """Evaluate the power at the given coordinates."""
        data_array = self._get_empty_data_array(az_grid.shape, beam_type="power")
        for pol_i in np.arange(self.Npols):
            data_array[0, pol_i, :, :] = np.cos(za_grid) ** 2
        return data_array
