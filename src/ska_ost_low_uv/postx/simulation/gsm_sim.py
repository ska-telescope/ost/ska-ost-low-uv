"""gsm_sim: Simulation tools using pygdsm diffuse sky model."""

from __future__ import annotations

import typing

if typing.TYPE_CHECKING: # pragma: no cover
    from ..aperture_array import ApertureArray
import healpy as hp
import numpy as np
import xarray as xr
from astropy.coordinates import EarthLocation
from matvis import simulate_vis
from pyuvdata.analytic_beam import AnalyticBeam, UnpolarizedAnalyticBeam
from ska_ost_low_uv.vis_utils import vis_arr_to_matrix

from ..coords.coord_utils import hpix2sky
from ..simulation.beam_models import SimpleCosSqBeam


def simulate_visibilities_gsm(
    aa: ApertureArray,
    beam: AnalyticBeam = None,
    n_side: int = 32,
) -> xr.DataArray:
    """Use pygdsm + matvis to simulate visibilites, add in Sun."""
    f_mhz = aa.uvx.data.frequency[0] / 1e6

    aa.gsm.generate(f_mhz)

    # Generate sky model flux with GSM
    sm = aa.gsm.observed_gsm
    sm64 = hp.ud_grade(sm, n_side)
    sky_model = np.expand_dims(sm64[~sm64.mask], axis=1)

    # Compute corresponding RA/DEC
    sky_coord = hpix2sky(n_side, np.arange(len(sm64)))
    sky_coord = sky_coord[~sm64.mask].icrs
    ra = sky_coord.ra.to('rad').value
    dec = sky_coord.dec.to('rad').value

    if beam is None:
        beams = [SimpleCosSqBeam()]
    else:
        beams = [beam]

    ants = dict(zip(np.arange(len(aa.xyz_enu)), aa.xyz_enu))

    is_polarized =  not isinstance(beam, UnpolarizedAnalyticBeam)

    # vis_vc has shape (NFREQ, NTIMES, NBLS) - unpolarised
    # if polarized     (NFREQ, NTIMES, NBLS, NFEED, NFEED)
    vis_vc = simulate_vis(
        ants=ants,
        fluxes=sky_model,
        ra=ra,
        dec=dec,
        freqs=np.array([f_mhz * 1e6]),
        times=aa.t,
        beams=beams,
        polarized=is_polarized,
        precision=1,
        telescope_loc=aa.earthloc,
    )

    if is_polarized:
        V = vis_vc.reshape((1, 1, aa.n_ant, aa.n_ant, 4))
    else:
        # Convert into matrix - NFREQ = NTIMES = 1
        _V = vis_vc.reshape((1, 1, aa.n_ant, aa.n_ant))

        # Convert to 4-pol
        V = np.zeros(shape=(1, 1, aa.n_ant, aa.n_ant, 4), dtype='complex64')
        _V0 = np.zeros(shape=(aa.n_ant, aa.n_ant), dtype='complex64')

        V[..., 0] = _V
        V[..., 1] = _V0
        V[..., 2] = _V0
        V[..., 3] = _V

    # Convert to xarray
    V = xr.DataArray(V, dims=('time', 'frequency', 'ant1', 'ant2', 'polarization'))

    # Add in Sun - TODO
    # Need to get units to match (Jy vs K)
    # sun = sun_model(aa)
    # sky_model = {'sun': sun}
    # v_sun = simulate_visibilities(aa, sky_model=sky_model)

    return V
