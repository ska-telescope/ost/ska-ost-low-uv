"""Test aa_simulation plotting routines."""

import pylab as plt
import pytest
from ska_ost_low_uv.io import hdf5_to_uvx
from ska_ost_low_uv.postx import ApertureArray
from ska_ost_low_uv.utils import get_test_data

from pyuvdata.analytic_beam import ShortDipoleBeam
from ska_ost_low_uv.postx.simulation.beam_models import SimpleCosSqBeam

uvx = hdf5_to_uvx(
    get_test_data('aavs2_1x1000ms/correlation_burst_204_20230823_21356_0.hdf5'),
    telescope_name='aavs2',
)
aa = ApertureArray(uvx)


@pytest.mark.mpl_image_compare
def test_orthview():
    """Test plotting routines."""
    fig = plt.figure('orth')
    aa.simulation.orthview_gsm()
    return fig


@pytest.mark.mpl_image_compare
def test_mollview():
    """Test plotting routines."""
    fig = plt.figure('orth')
    aa.simulation.mollview_gsm()
    return fig

def test_gsm_sim():
    """Test all-sky simulation."""
    vis = aa.simulation.sim_vis_gsm()
    # Polarized beam
    vis = aa.simulation.sim_vis_gsm(beam=ShortDipoleBeam())
    # Unpolarized beam
    vis = aa.simulation.sim_vis_gsm(beam=SimpleCosSqBeam())

if __name__ == '__main__':
    test_orthview()
    test_mollview()
    test_gsm_sim()
