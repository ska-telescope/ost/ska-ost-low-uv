"""test_satellites.py -- test satellite TLE loading."""
import numpy as np
import pylab as plt
from ska_ost_low_uv.io import read_uvx
from ska_ost_low_uv.postx import ApertureArray
from ska_ost_low_uv.postx.coords.satellites import (
    compute_satellite_altaz,
    compute_satellite_radec,
    load_tles,
)
from ska_ost_low_uv.utils import get_test_data


def test_satellites():
    """Test satellite TLE loading."""
    tles = load_tles(get_test_data('satellites/tles_2025.01.22.txt'))
    print(tles)
    for name in ('CATCH-1', 'SVOM', 'STARLINK-31239'):
        assert name in tles.keys()

    uvx = read_uvx(get_test_data('satellites/s81-svom.uvx'))
    aa = ApertureArray(uvx)

    altaz = compute_satellite_altaz(aa, satellite=tles['SVOM'])
    radec = compute_satellite_radec(aa, satellite=tles['SVOM'])
    print(radec)
    print(altaz)

    ra_known = 325.94274827
    dec_known = 23.23994588
    assert np.isclose(radec.ra.to('deg').value, ra_known)
    assert np.isclose(radec.dec.to('deg').value, dec_known)

    aa.viewer.skycat = tles
    aa.imaging.make_image()
    aa.viewer.orthview(overlay_srcs=True)

    aa.imaging.make_healpix()
    aa.viewer.mollview(overlay_srcs=True)
    plt.show()


if __name__ == "__main__":
    test_satellites()
