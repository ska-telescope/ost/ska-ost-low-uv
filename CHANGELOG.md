# Version History

## 0.0.0 (29/08/2024)

* Moved repository from github -> gitlab

## 0.0.3 (30/10/2024)

* Fixed RECEPTOR_ANGLE keyword for CASA export
